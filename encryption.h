#include <stdio.h>
void handleErrors(void);
int encrypt_text(unsigned char *plaintext, int plaintext_len,  unsigned char *ciphertext);
int decrypt_text(unsigned char *ciphertext, int ciphertext_len, unsigned char *plaintext);
void init_openssl();
void deinit_openssl();
void test_encryption(void);
