all: pbproxy.o socket_prog.o encryption.o
	gcc socket_prog.o pbproxy.o encryption.o -lssl -lcrypto -o pbproxy

pbproxy.o: pbproxy.c socket_prog.c socket_prog.h encryption.c encryption.h
	gcc -c pbproxy.c

encryption.o: encryption.c encryption.h
	gcc -c -lssl -lcrypto encryption.c

socket_prog.o: socket_prog.c socket_prog.h
	gcc -c socket_prog.c

clean:
	rm pbproxy encryption.o socket_prog.o
