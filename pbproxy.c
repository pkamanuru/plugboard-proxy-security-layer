#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "socket_prog.h"
#include "encryption.h"

#define BUFFER_SIZE 4096
extern int errno;

int main(int argc, char *argv[]){

  //  connect_and_test();
  //Open a connection with ssh
  //  int std_in_fd = fileno(stdin);
  /*  for ( int i=0; i<argc; i++ )
      dprintf(STDERR_FILENO, "%s\n", argv[i]);*/
  if ( argc > 3 && strcmp(argv[3], "-l") == 0 ){

    //server mode
    char *ip = argv[5];    int portno = atoi(argv[6]); int bind_port = atoi(argv[4]);

      int sfd = bind_socket(bind_port);
      while (1){
        int sockfd = connect_to_ssh(ip, portno);
        int client_connected_socket = accept_connection(sfd);
        int fds[2] = { sockfd, client_connected_socket};
        int no_of_descriptors = 2;
        struct pollfd *socket_fds = prepare_stdin_for_poll(fds, no_of_descriptors);
        poll_on_sockets( socket_fds, no_of_descriptors, -1);
        close(client_connected_socket);
        close(sockfd);
        free(socket_fds);
      }
    }

   else{
     char *ip = argv[1];
     int portno = atoi(argv[2]);
     dprintf(STDERR_FILENO, "Connecting to Server:%s, portno:%d\n", ip, portno);
     int sockfd = connect_to_ssh( ip, portno );
     dprintf( STDERR_FILENO, "Connected To Server\n" );
     //Buffer to read from stdin
     //STDIN, Network Socket
     int no_of_descriptors=2;
     int fds[2] = { STDIN_FILENO, sockfd };
     struct pollfd *socket_fds = prepare_stdin_for_poll(fds, no_of_descriptors);
     poll_on_sockets(socket_fds, no_of_descriptors, STDOUT_FILENO);

    }
  return 0;

}

/*void connect_and_test(){
  int sockfd = socket (AF_INET, SOCK_STREAM, 0);
  if ( sockfd < 0 )
    printf("Could not create socket\n");

  struct sockaddr_in serv_addr; int portno=130;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(portno);
  inet_aton("127.0.0.1", (struct in_addr *)&serv_addr.sin_addr.s_addr);
  if ( connect(sockfd, (const struct sockadrr *)&serv_addr, sizeof(struct sockaddr_in) ) < 0 )
    printf("connect failed\n");
  char buffer[BUFFER_SIZE];
  read(sockfd, buffer, BUFFER_SIZE);
  printf("%s", buffer);
  for ( int i=0; i<BUFFER_SIZE; i++ )
    buffer[i] = '\0';

  read(sockfd, buffer, BUFFER_SIZE);
  printf("%s", buffer);
  for ( int i=0; i<BUFFER_SIZE; i++ )
    buffer[i] = '\0';

  read(sockfd, buffer, BUFFER_SIZE);
  printf("%s", buffer);
  for ( int i=0; i<BUFFER_SIZE; i++ )
    buffer[i] = '\0';

  read(sockfd, buffer, BUFFER_SIZE);
  printf("%s", buffer);
  for ( int i=0; i<BUFFER_SIZE; i++ )
    buffer[i] = '\0';


}*/
