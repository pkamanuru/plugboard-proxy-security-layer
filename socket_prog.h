#include <stdio.h>
int connect_to_ssh( char *ip, int portno);
struct pollfd *prepare_stdin_for_poll(int *fds, int n);
void poll_on_sockets(struct pollfd *std_in, int no_of_desc, int fd);
int bind_socket(int portno);
int accept_connection(int fd);
