#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <poll.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "encryption.h"

#define BUFFER_SIZE 4096
extern int errno;
int bind_socket( int portno ){
  char *all_interfaces="0.0.0.0";  int sfd;

  sfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sfd == -1)
    dprintf(STDERR_FILENO, "socket creation failed %s\n", strerror(errno));

  if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    dprintf(STDERR_FILENO, "setsockopt(SO_REUSEADDR) failed");

  dprintf(STDERR_FILENO, "socket created\n");

  struct sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(portno);
  //inet_aton( all_interfaces, (struct in_addr *)&serv_addr.sin_addr.s_addr );
  inet_pton(AF_INET, all_interfaces, &(serv_addr.sin_addr));


  if ( bind(sfd, (struct sockaddr *)&serv_addr,  sizeof serv_addr) < 0 )
    dprintf( STDERR_FILENO, "bind failed:%s\n", strerror(errno) );

  dprintf(STDERR_FILENO, "Bind success\n");
  return sfd;
}

int accept_connection(int sfd){

  struct sockaddr_in peer_addr;
  socklen_t peer_addr_size;

  if ( listen(sfd, 5) < 0 )
    dprintf(STDERR_FILENO, "listen failed:%s\n", strerror(errno) );
  dprintf(STDERR_FILENO, "Calling accept\n");
  int connected_socket = accept( sfd, (struct sockaddr *)&peer_addr, &peer_addr_size );
  if ( connected_socket < 0 ){
    dprintf(STDERR_FILENO, "accept failed:%s\n", strerror(errno) );
  }
  dprintf(STDERR_FILENO, "Accepted Connection\n");
  return connected_socket;

}


int connect_to_ssh( char *ip, int portno){

  int sockfd = socket (AF_INET, SOCK_STREAM, 0);
  if ( sockfd < 0 )
    dprintf( STDERR_FILENO, "Could not create socket\n");

  //SSH Address to Connect Socket to 127.0.0.1:130
  struct sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(portno);
  inet_aton( ip, (struct in_addr *)&serv_addr.sin_addr.s_addr );
  dprintf(STDERR_FILENO, "Calling connect\n");
  if ( connect(sockfd, (const struct sockadrr *)&serv_addr, sizeof(struct sockaddr_in) ) < 0 )
    dprintf( STDERR_FILENO, "an error: %s\n", strerror(errno) );
  return sockfd;

}

struct pollfd *prepare_stdin_for_poll(int *fds, int n){
  struct pollfd *std_in = ( struct pollfd *) malloc ( n*sizeof(struct pollfd) );

  for ( int i=0; i<n; i++ ){
    std_in[i].fd = fds[i];
    std_in[i].events |= POLLIN | POLLPRI;
    std_in[i].revents = 0;
  }

  return std_in;
}

void poll_on_sockets(struct pollfd *fds, int no_of_fds, int fd){

  dprintf(STDERR_FILENO, "Polling Started\n");
  char buffer[BUFFER_SIZE];

  while(1){
    //dprintf(STDERR_FILENO, "Calling Poll\n");
    int ret = poll ( fds, no_of_fds, -1);
    if ( ret < 0 )
      dprintf(STDERR_FILENO, "POLL Error %s\n", strerror(errno) );

    //dprintf(STDERR_FILENO, "Coming out of Poll ret value:%d\n", ret);
    for ( int i=0; i<no_of_fds; i++ ){

      //dprintf(STDERR_FILENO, "POLL OUT %d returned events %d\n", std_in[i].fd, std_in[i].revents);
      if ( i == 1 && ( fds[i].revents & POLLHUP ) ){
        dprintf( STDERR_FILENO, "Remote Hung up/ Shut down\n");
        return;
      }

      if ( fds[i].revents & POLLIN || fds[i].revents & POLLPRI ){
        //        dprintf(STDERR_FILENO, "About to read\n");

        /*int len = read( fds[i].fd , buffer, BUFFER_SIZE );*/
        /*        if ( len == BUFFER_SIZE ){
          dprintf(STDERR_FILENO, "Buffer full\n");
          exit(1);
          }*/
        //Read from Socket and Write to Standard Output and STDERR
        if ( i == 1 ){

          char decrypted_text[4096];
          int len = recv( fds[i].fd, buffer, 4, MSG_WAITALL);
          int *temp = buffer;
          int packet_length = *temp;
          int new_len = recv( fds[i].fd, buffer, packet_length, MSG_WAITALL);
          int dec_len = decrypt_text( buffer, new_len, decrypted_text);

          if ( fd != -1 )
            write( fd, decrypted_text, dec_len);
          else
            write( fds[0].fd, decrypted_text, dec_len);

          /*   char *buffer_position = buffer;
          int no_of_bytes_left = len;
          int *message_len = (int *)buffer_position;
          char decrypted_text[4096];
          while( no_of_bytes_left > 0 ){
            int dec_len = decrypt_text( buffer_position+4, *message_len, decrypted_text);
            if ( fd != -1 )
              write( fd, decrypted_text, dec_len);
            else
              write( fds[0].fd, decrypted_text, dec_len);

            buffer_position = buffer + 4 + *message_len;
            no_of_bytes_left = no_of_bytes_left - 4 - *message_len;
            message_len = ( int *) buffer_position;
            }*/

        }

        //Read from Standard Input and Write to Socket
        else{
          //dprintf(STDERR_FILENO, "Message from STDIN\n");
          //write(STDERR_FILENO, buffer, len);
          //dprintf(STDERR_FILENO, "\n");

          int len = read( fds[i].fd , buffer, BUFFER_SIZE );
          //encrypt before writing
          char new_buffer[5096];
          int new_len = encrypt_text( buffer, len, new_buffer+4);
          int *val = (int *) new_buffer;
          *val = new_len;
          //write( fds[1].fd, buffer, len);
          //dprintf(STDERR_FILENO, "Writing %d bytes\n", new_len+4);
          write( fds[1].fd, new_buffer, new_len+4);
        }

        //Clear Buffer
        for ( int i=0; i<BUFFER_SIZE; i++ )
          buffer[i] = '\0';
      }

      fds[i].revents = 0;

    }

  }

}
